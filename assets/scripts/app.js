'use strict';

var app = angular.module("flexbase", ['ngRoute']);

app.config(["$interpolateProvider", function($interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
}]);


/**
 *    IF ROUTING IS NEEDED
 **/

// app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
//   $routeProvider
//    .when('/', {
//     templateUrl: 'templates/login.html',
//     controller: 'LoginController',
//     controllerAs: 'ctrl'
//   })
//   ;
//   // configure html5 to get links working on jsfiddle
//   $locationProvider.html5Mode(true);
// }]);

app.run(['$rootScope', '$window',
  function($rootScope, $window) {

  $rootScope.user = {};

  /**
   *    IF FACEBOOK IS NEEDED
   **/

  // $window.fbAsyncInit = function() {
  //   // Executed when the SDK is loaded
  //   FB.init({
  //     appId: 1234567890
  //     channelUrl: 'app/channel.html',
  //     status: true,
  //     cookie: true,
  //     xfbml: true,
  //     version: 'v2.4'
  //   });
  //   FB.Canvas.setAutoGrow();
  // };

  // (function(d){
  //   // load the Facebook javascript SDK

  //   var js,
  //   id = 'facebook-jssdk',
  //   ref = d.getElementsByTagName('script')[0];

  //   if (d.getElementById(id)) {
  //     return;
  //   }

  //   js = d.createElement('script');
  //   js.id = id;
  //   js.async = true;
  //   js.src = "https://connect.facebook.net/en_US/sdk.js";

  //   ref.parentNode.insertBefore(js, ref);

  // }(document));

}]);

