<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Alpha\ModelProvider;

class FrontController extends AbstractController
{
    private $twig;

    public function __construct(Environment $twig, ModelProvider $models)
    {
        $this->twig = $twig;
        $this->models = $models;
    }


    public function index(Request $request): Response
    {
        $user = ["login" => "smwhr"];

        $userProvider = ($this->models)("User");
        $user = $userProvider->get(3);

        return $this->render('index.html.twig', [
            'user' => $user,
        ]);
    }
}